﻿using Abp.Dependency;
using System.Collections.Generic;

namespace Abp.RemoteEventBus.Kafka
{
    public class KafkaSetting : IKafkaSetting, ITransientDependency
    {
        public Dictionary<string, object> Properties { get; set; }

        public KafkaSetting()
        {
            Properties = new Dictionary<string, object>();
            Properties.Add("enable.auto.commit", "true");
            Properties.Add("auto.commit.interval.ms", "1000");
            Properties.Add("session.timeout.ms", "30000");
            Properties.Add("commit.period", "5");
            
        }
    }
}
