﻿using System.Collections.Generic;

namespace Abp.RemoteEventBus.Kafka
{
    public interface IKafkaSetting
    {
        Dictionary<string, object> Properties { get; set; }
    }
}
