﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;
using MyExchange.Api.TradeExpert;
using MyExchange.Web.TradeExpert;

namespace MyExchange.Base.TradeExpert
{
    /// <summary>
    /// Базовый класс для экспертов дающих советы по запросам тактик торговли
    /// </summary>
    public abstract class TradeAdviserBase<TContext> : ApplicationService, ITradeAdviser<TContext>
        where TContext : AdviserContext
    {
        public TContext Context { get; set; }

        /// <summary>
        /// Перечень типов стратегий, для которых наиболее приспособлен
        /// эксперт и может давать советы для торговых тактик
        /// </summary>
        public virtual IList<TradingTypes> TradingTypeList { get; }
        
        public string Id { get; }

        public virtual string BriefName { get; protected set; }

        public virtual string FullName { get; protected set; }

        public virtual string Url { get; protected set; }

        public virtual string Description { get; protected set; }
        
        protected TradeAdviserBase() 
        {
            TradingTypeList = new List<TradingTypes>();
            Id = this.GetType().Name;
        }

        public abstract Task<TradeExpertAdvice> GetAdviceAsync(AdviserContext context);
    }
}
