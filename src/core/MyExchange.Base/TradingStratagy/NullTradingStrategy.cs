﻿using System;
using JetBrains.Annotations;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.TradingStrategy;

namespace MyExchange.Web.ExchangeStratagy
{
    /// <summary>
    /// К этой стратегии прикрепляются боты, котрые работают автономно без управления
    /// какой либо значимой стратегией.
    /// </summary>
    public class NullTradingStrategy : TradingStrategyBase<TradingStrategyContextBase>
    {
        public override string Name { get; } = "Фейковая стратегия-заглушка";
        public override Action Execute { get; } = Implementation;
        public override void Dispose()
        {
            throw new NotImplementedException();
        }

        private static void Implementation()
        {
            ///Логика управления ботами для достижения цели стратегии
            ///Она должна быть очень проста: запускаются все боты и работают автономно

        }
    }
}
