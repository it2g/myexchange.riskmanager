﻿using System;
using System.Collections.Generic;
using Abp.Application.Services;
using JetBrains.Annotations;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.TradingStrategy;
using MyExchange.Api.TradingTactic;
using MyExchange.Base.BotTactic;

namespace MyExchange.Web.ExchangeStratagy
{
    /// <summary>
    /// Базовая стратегия автоматизированной торговли на биржах, применяемая системой
    /// </summary>
    public abstract class TradingStrategyBase<TContext> : BotTacticBase<TContext>, ITradingStrategy<TContext>
        where TContext : TradingStrategyContextBase
    {
        protected IList<ITradingTactic> TradingTactics { get; }
        
        public TradingStrategyContextBase GetContext()
        {
            return Context;
        }
    }


}
