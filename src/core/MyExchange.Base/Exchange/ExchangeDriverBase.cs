﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Abp.Application.Services;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Exchange;
using MyExchange.Api.Extentions;
using MyExchange.Api.Services.Bl;

namespace MyExchange.Web.Exchange
{
    public abstract class ExchangeDriverBase : ApplicationService
    {
        protected IExchangeUserKeyProvider ExchangeUserKeyProvider { get; }

        protected ExchangeDriverBase([NotNull] IExchangeUserKeyProvider exchangeUserKeyProvider)
        {
            ExchangeUserKeyProvider = exchangeUserKeyProvider ?? throw new ArgumentNullException(nameof(exchangeUserKeyProvider));
        }
        
        /// <summary>
        /// С бирж приходят сведения о датах в различных исчислениях и их нужно приводить
        /// к DateTime. На разных биржах, это могут быть разные приведения.
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        protected virtual DateTime ConvertDateTime(long datetime)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(datetime);
        }

        /// <summary>
        /// Проверяет успешность http запроса
        /// </summary>
        /// <param name="response"></param>
        /// <param name="exchangeName"></param>
        protected bool CheckResponse(HttpResponseMessage response, string exchangeName)
        {
            if (response == null || response.StatusCode != HttpStatusCode.OK)
            {
                Logger.Error($"Сервер биржи {exchangeName} возвращает ошибку: {response?.StatusCode?? HttpStatusCode.BadRequest}");
                Debug.WriteLine($"Сервер биржи {exchangeName} возвращает ошибку: {response?.StatusCode ?? HttpStatusCode.BadRequest}");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Метод определяющий тип Order'а
        /// </summary>
        /// <param name="orderType"> строка содержащая тип Order'а</param>
        /// <returns>Возвращает тип Order'а</returns>
        protected TradeTypes IdentifyOrderType(string orderType)
        {
            return orderType.ToLower().Contains("buy") ? TradeTypes.Buy : TradeTypes.Sell;
        }

        /// <summary>
        /// Преобразование в структуру валютной пары из строки. Например: "BTC_USD"
        /// </summary>
        /// <param name="stringPair"></param>
        /// <param name="pairSplitter"></param>
        /// <returns></returns>
        protected PairDto GetPairFromString(string stringPair, string pairSplitter)
        {
            string[] currencyNames = stringPair.Split(pairSplitter.ToCharArray());

            var pair = new PairDto
            {
                Currency1 = new CurrencyDto { BrifName = currencyNames[0], Id = Guid.Empty },
                Currency2 = new CurrencyDto { BrifName = currencyNames[1], Id = Guid.Empty},
                Id = Guid.Empty,
                Name = stringPair
            };

            return pair;
        }

        /// <summary>
        /// Шифруем сообщение по ключу
        /// </summary>
        /// <param name="key">ключ шифрования</param>
        /// <param name="message">строка, которая будет зашифрована</param>
        /// <returns></returns>
        protected string Sign(string key, string message)
        {
            using (HMACSHA512 hmac = new HMACSHA512(Encoding.UTF8.GetBytes(key)))
            {
                byte[] b = hmac.ComputeHash(Encoding.UTF8.GetBytes(message));

                return b.ConvertToString();
            }
        }

        /// <summary>
        /// Формируем запрос из всех строк
        /// TODO комментарий не проясняят назначения метода, а его название слишком обобщенное
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected string ToQuery(IDictionary<string, string> parameters)
        {
            var queries = new List<string>();

            foreach (var parameter in parameters)
            {
                queries.Add(HttpUtility.UrlEncode(parameter.Key) + "=" + HttpUtility.UrlEncode(parameter.Value));
            }

            var query = string.Join("&", queries);

            return query;
        }

        /// <summary>
        /// Получить дату и время в миллисекундах
        /// </summary>
        /// <returns></returns>
        protected static long GetTimeStamp()
        {
            var d = (DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds;
            return (long)d;
        }

        /// <summary>
        /// Получить ключи пользователя на бирже
        /// </summary>
        /// <param name="exchange"></param>
        /// <returns></returns>
        protected async Task<UserAndKeys> GetExchangeUserKeysAsync(Api.Exchange.Exchange exchange)
        {
            //TODO исправить недочет  с идентификатором пользователя
            var keyPair = await ExchangeUserKeyProvider.GetKeyAsync(exchange.Id);

            if (keyPair == null) throw new Exception("Пользователь с указанным параметрами не найден");

            var andKeys = new UserAndKeys
            {
                ApiSecret = keyPair.ApiSecretKey,
                Key = keyPair.ApiOpenKey,
                ExchangeUserId = keyPair.ExchangeUserId
            };

            return andKeys;
        }

        /// <summary>
        /// Стуруктура для хранения ключей (api) пользователя. Специфичная для этой биржи
        /// </summary>
        public struct UserAndKeys
        {
            public Guid ExchangeUserId { get; set; }
            public string ApiSecret { get; set; }
            public string Key { get; set; }
        }

    }
}
