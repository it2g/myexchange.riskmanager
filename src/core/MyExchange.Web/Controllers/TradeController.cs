﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Services;
using System;
using System.Threading.Tasks;
using MyExchange.Api.Dto.Trade;
using MyExchange.Services.Crud;

namespace MyExchange.Web.Controllers
{
    [Route("api/[controller]")]
    public class TradeController : MyExchangeControllerBase<TradeCrudAppService>
    {
        public TradeController(TradeCrudAppService crudService) : base(crudService)
        {
        }

        [HttpGet]
        public async Task<PagedResultDto<TradeDto>> Get(TradePagedResultRequestDto input)
        {

            return await Service.GetAll(input);
        }

        [HttpGet("{id}")]
        public async Task<TradeDto> Get([FromRoute]Guid id)
        {
            return await Service.Get(id);
        }

       
    }
}
