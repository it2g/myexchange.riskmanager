using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Services.Crud;
using MyExchange.Api.Services.Infrastructure;
using MyExchange.Services.Bl;

namespace MyExchange.Test
{
    [TestClass]
    public class BarServiceTests
    {
        private ITradeCrudAppService _tradeCrudAppService;
        /// <summary>
        /// ��������� ������������� ���������� ������ �� ������������ 
        /// ���������� �������
        /// </summary>
        [TestMethod]
        public void GetBarsForFrameTime()
        {
            var timeEnd = DateTime.UtcNow;
            var timeBegin = timeEnd - TimeSpan.FromMinutes(20);
            var timer = A.Fake<ITimer>();
            _tradeCrudAppService = A.Fake<ITradeCrudAppService>();
            A.CallTo(() => _tradeCrudAppService.GetAll(A<TradePagedResultRequestDto>.Ignored))
                .Returns(
                    Task.FromResult(
                        new PagedResultDto<TradeDto>()
                        {
                            Items = GenerateFakeTrades(20, timeBegin, timeEnd),
                            TotalCount = 20
                        }
                    )
                );
            //���������� ������
            var barService = new BarFromTradeProvider(_tradeCrudAppService, timer);
            //���������� �����
            var barScale = TimeSpan.FromMinutes(4);
            var bars = barService.GetList(Guid.Empty, "...", 
                                barScale, timeBegin, timeEnd).Result;
            //������� ��������� ���� ������
            Assert.AreEqual(5, bars.Count);
        }

        /// <summary>
        /// ��������� ������������ ���������� ������ 
        /// </summary>
        [TestMethod]
        public void GetBarsByRequiredCount()
        {
            ///��������� ���������� ������
            var barCounts = 5;
           
            // ������ ����� (���� �����)
            var barScale = TimeSpan.FromMinutes(4);
            var timer = A.Fake<ITimer>();
            _tradeCrudAppService = A.Fake<ITradeCrudAppService>();
            A.CallTo(() => _tradeCrudAppService.GetAll(A<TradePagedResultRequestDto>.Ignored))
                .Returns(
                    Task.FromResult(
                        new PagedResultDto<TradeDto>()
                        {
                            Items = GenerateFakeTrades(20, DateTime.UtcNow - barCounts * barScale, DateTime.UtcNow),
                            TotalCount = 20
                        }
                    )
                );
            //���������� ������
            var barService = new BarFromTradeProvider(_tradeCrudAppService, timer);
            //���������� �����
            
            var bars = barService.GetList(Guid.Empty, "...",
                barScale, barCounts).Result;
            //������� ��������� ���� ������
            Assert.AreEqual(5, bars.Count);
        }

        private List<TradeDto> GenerateFakeTrades(int tradeCount, DateTime timeBegin, DateTime timeEnd)
        {
            Random rnd = new Random();

            var trades = new List<TradeDto>();

            var timeDelta = (timeEnd - timeBegin) / tradeCount;

            var slider = timeBegin;

            for (var i = 1; i <= tradeCount; i++)
            {
                var trade = new TradeDto()
                {
                    Id = Guid.Empty,
                    TradeTypes = TradeTypes.Buy,
                    Quantity = Convert.ToDecimal(rnd.Next(1, 10)),
                    ExchangeId = "...",
                    PairId = Guid.Empty,
                    OrderId = null,
                    TradeId = ""
                };

                trade.Price = Convert.ToDecimal(rnd.Next(1, 100));

                trade.Amount = trade.Price * trade.Quantity;

                trade.Date = slider;
                
                trades.Add(trade);

                slider += timeDelta;
            }

            return trades;

        }
    }
}
