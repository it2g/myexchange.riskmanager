﻿using MyExchange.Api.TradeExpert;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Совет эксперта в отношении проведения сделки
    /// На статистике таких советов и ретрообзоре можно делать выводы
    /// о ликвидности того или иного эксперта (его способности давать эффективные советы)
    /// </summary>
    public class TradeExpertAdvice : EntityBase
    {
        /// <summary>
        /// Оценка поведения рынка
        /// </summary>
        public Advices Advice { get; set; }

        /// <summary>
        /// Вероятность сделанной оценки по мнению эксперта
        /// </summary>
        public double Possibility { get; set; }

    }
}
