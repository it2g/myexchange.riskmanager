﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Entities;

namespace MyExchange.Api.Entity
{
    /// <summary>
    /// Справочник валютных пар
    /// </summary>
    public class Pair : EntityBase, ISoftDelete
    {
        public Guid Currency1Id { get; set; }
        public virtual Currency Currency1 { get; set; }

        public Guid Currency2Id { get; set; }
        public virtual Currency Currency2 { get; set; }

       
        // Склеивает две валюты в единую пару
        public string GetSystemName(string splitter)
        {
            return Currency1.BrifName + splitter + Currency2.BrifName;
        }

        public bool IsDeleted { get; set; }
    }
}
