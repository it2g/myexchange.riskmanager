﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;

namespace MyExchange.Api.BotTactic
{
    /// <summary>
    /// Менеджер, котрый управляет ботами
    /// Он используется в Административной консоли для управления и мониторинга ботами.
    /// В автоматизированом режиме работы системы этот менеджер может использоваться и стратегией 
    /// самой торговой системы
    /// Под управлением понимается: запуск, остановка и установка на паузу ботов
    /// На самом деле все эти действия применяются к тактикам, а бот действует как оболочка
    /// для управления всеми этими действиями
    /// </summary>
    public interface IBotManager<TBotTactic, TBotTacticContext> : IApplicationService
        where TBotTacticContext : BotTacticContextBase
        where TBotTactic : IBotTactic<TBotTacticContext>
    {

        /// <summary>
        /// Запускает фоновый алгоритм, реализующий ботом указанную тактику
        /// </summary>
        Task Start(TBotTactic bot);

        /// <summary>
        /// Останавливает указанный бот
        /// </summary>
        Task StopBot(TBotTactic bot);

        /// <summary>
        /// Приостанавливает  ботом
        /// </summary>
        /// <param name="bot"></param>
        Task PauseBot(TBotTactic bot);

        /// <summary>
        /// Продолжение выполнения ранее остановленного бота
        /// </summary>
        /// <param name="bot"></param>
        Task ResumeBot(TBotTactic bot);
        
        /// <summary>
        /// Восстанавливает прерванные по непредвиденной причине работающие боты
        /// При этом информация о запущенных ботах берется из БД
        /// </summary>
        /// <param name="bot"></param>
        IList<TBotTactic> RestoreBots();

        /// <summary>
        /// Изменение количества средств, используемых ботом для торговли активами
        /// Требуется дополнительное обдумывание этого метода как с помощью него влиять на 
        /// сумму, выделенную ранее боту
        /// </summary>
        /// <param name="value"></param>
        Task ChangeValue(TBotTactic bot, decimal value);

     
    }
}
