﻿using System.Collections.Generic;
using Abp.Application.Services;

namespace MyExchange.Api.TradeExpert
{
    /// <summary>
    /// Этот механизм позволяет получить перечень советчиков, наиболее
    /// подходящих по указанным параметрам торговоли
    /// </summary>
    public interface IAdviserProvider : IApplicationService
    {
        IList<ITradeAdviser> GetAdvisers(IDictionary<string, object> parametrs);

    }
}
