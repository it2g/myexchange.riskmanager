﻿using System;
using System.Threading.Tasks;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Constants;
using MyExchange.Api.Entity;

namespace MyExchange.Api.TradingTactic
{
    /// <summary>
    /// Интерфейс для торгующих тактик
    /// </summary>
    public interface ITradingTactic : IBotTactic
    {
        /// <summary>
        /// Изменяет объем валюты, которым может оперировать тактика в процессе реализации
        /// торгового алгоритма. 
        /// Тот, кто будет вызывать данный метод, должен перехватывать Exceptions
        /// для анализа пиричны отказа в изменении значений и информаировании об этом трейдера
        /// TODO нужно подумать, что не всем тактикам бота нужна эта валюта
        /// и как следствие - само значение и метод его измененияя.
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="value"></param>
        /// <param name="purpose"></param>
        void ChangeCurrencyValue(Currency currency, decimal value, string purpose);
    }

    public interface ITradingTactic<TContext> : ITradingTactic, IBotTactic<TContext> 
        where TContext : BotTacticContextBase
    {
    }


}
