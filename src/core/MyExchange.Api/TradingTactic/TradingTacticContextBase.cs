﻿using System;
using System.Collections.Generic;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Dto.Signal;
using MyExchange.Api.Entity;
using MyExchange.Api.Exchange;
using MyExchange.Api.TradeExpert;
using MyExchange.Api.TradingStrategy;

namespace MyExchange.Api.TradingTactic
{
    /// <summary>
    /// Контекст, которым оперирует тактика, выполняемая ботом операции на бирже
    /// Этим контекстом оперируют большая часть тактик, но он может быть расширен наследованием
    ///  </summary>
    public abstract class TradingTacticContextBase : BotTacticContextBase
    {
        /// <summary>
        /// Объем денежных средств выделеных тактике для ведения торговли.
        /// Он резервирует с одной стороны денежные средства на кошельке, 
        /// и ограничивает бота, с другой стороны, в использовании общедоступных денежных средств
        /// </summary>
        public CurrencyValue CurrencyValue { get; set; }

        /// <summary>
        /// С этой пары нужно начинать торги,
        /// </summary>
        public Guid PairId { get; set; }

        public string ExchangeId { get; set; }

        public Guid TradingStrategyContextId { get; set; }
        public string TradingStrategyId { get; set; }

        /// <summary>
        /// Индикаторы, которыми оперирует тактика для отслеживания сигналов
        /// на изменение тренда. Список индикаторов через запятую состоит из их идентификаторов.
        /// Идентификаторами являются наиметования типов индикаторов.
        /// TODO Подумать, как сообщить тактике об этих индикаторах
        /// не добавляя зависимости от этих индикаторов. Скорее всего это можно сделать
        /// через настройки, или через пользовательский админский интерфес
        /// </summary>
        public string TradeIndicators { get; set; }

        public IList<SignalDto> Signals { get; set; }
    }

   
}
