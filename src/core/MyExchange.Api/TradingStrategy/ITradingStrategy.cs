﻿using System;
using Abp.Application.Services;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;

namespace MyExchange.Api.TradingStrategy
{
    public interface ITradingStrategy : IBotTactic
    {
        TradingStrategyContextBase GetContext();
    }

    public interface ITradingStrategy<TContext> : ITradingStrategy, IBotTactic<TContext>
        where TContext : TradingStrategyContextBase
    {
    }
}
