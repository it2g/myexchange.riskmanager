﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.BotTactic;

namespace MyExchange.Api.TradingStrategy
{
    /// <summary>
    /// Менеджер, котрый управляет стратегиями
    /// </summary>
    public interface ITradingStrategyManager<TTradingStrategy, TTradingStrategyContext> : IApplicationService
        where TTradingStrategyContext : TradingStrategyContextBase
        where TTradingStrategy : ITradingStrategy<TTradingStrategyContext>
    {
        Task Start(TTradingStrategy strategy);

        /// <summary>
        /// Останавливает указанный бот
        /// </summary>
        Task Stop(TTradingStrategy strategy);

        /// <summary>
        /// Приостанавливает торговые операции ботом
        /// При этом политику в отношении уже выставленных заявок определяет стратегия бота
        /// </summary>
        /// <param name="strategy"></param>
        Task Pause(TTradingStrategy strategy);

        /// <summary>
        /// Продолжение выполнения ранее остановленного бота
        /// </summary>
        /// <param name="strategy"></param>
        Task Resume(TTradingStrategy strategy);

        /// <summary>
        /// Восстанавливает прерванные по непредвиденной причине работающие боты
        /// При этом информация о запущенных ботах берется из БД
        /// </summary>
        IList<TTradingStrategy> Restore();

        /// <summary>
        /// Изменение количества средств, используемых ботом для торговли активами
        /// Требуется дополнительное обдумывание этого метода как с помощью него влиять на 
        /// сумму, выделенную ранее боту
        /// </summary>
        /// <param name="value"></param>
        Task ChangeValue(TTradingStrategy strategy, decimal value);

     
    }
}
