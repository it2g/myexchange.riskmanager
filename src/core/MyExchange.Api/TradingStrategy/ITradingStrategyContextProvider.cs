﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyExchange.Api.TradingStrategy
{
    public interface ITradingStrategyContextProvider : IApplicationService
    {
        TradingStrategyContextBase Get(Guid contextId, string stratagyId);
    }
}
