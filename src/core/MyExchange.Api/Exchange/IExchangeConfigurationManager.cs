﻿namespace MyExchange.Api.Exchange
{
    /// <summary>
    /// Управляющий настройкой конфигурации системы
    /// </summary>
    public interface IExchangeConfigurationManager
    {
        /// <summary>
        /// В реестре регистрируется поднятый на какой либо машине агент.
        /// Этим реестром пользуется очередь для передачи запросов на биржи.
        /// </summary>
        /// <param name="ipAdress"></param>
        void RegistrateAgent(string ipAdress);
        
    }
}
