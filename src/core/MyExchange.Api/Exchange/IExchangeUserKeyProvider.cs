﻿using System.Threading.Tasks;
using MyExchange.Api.Data;

namespace MyExchange.Api.Exchange
{
    /// <summary>
    /// Предоставляет ключи пользователя для указанной биржи,
    /// чтобы обратиться к защищенным методам.
    /// </summary>
    public interface IExchangeUserKeyProvider
    {
        Task<ExchangeUserKeyPair> GetKeyAsync(string exchangeId);
    }
}
