﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;

namespace MyExchange.Api.Dto.Order
{
   public class OrderPagedResultRequestDto : PagedAndSortedResultRequestDto
    {
        /// <summary>
        /// Время, после которого нужно выгрузить все сделки до текущего момента
        /// </summary>
        public DateTime? LastRequestTime { get; set; }

        public string ExchangeId { get; set; }

        public Guid PairId { get; set; }
    }
}
