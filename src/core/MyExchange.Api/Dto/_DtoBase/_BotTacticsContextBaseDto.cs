﻿using System;
using MyExchange.Api.Constants;


namespace MyExchange.Api.Dto._DtoBase
{
    /// <summary>
    /// Контекст, которым оперирует тактика, выполняемая ботом операции на бирже
    /// Этим контекстом оперируют большая часть тактик, но он может быть расширен наследованием
    ///  </summary>
    public abstract class BotTacticContextBaseDto : DtoBase
    {
        /// <summary>
        /// Тактика, котрая будет использована для торговли на бирже.
        /// Через бота можно управлять этой тактикой
        /// TODO Т.к. контекст в конечном итоге являясь наследником этого
        /// базового типапредставляет собой уникальный тип, то скорее всего
        /// этот id может не понадобиться
        /// </summary>
        public Guid BotTacticId { get; set; }

        /// <summary>
        /// Используется менеджером ботов для мониторинга состояния бота
        /// </summary>
        public BotStatuses Status { get; set; }

        /// <summary>
        /// Дата и время запуска бота
        /// Если null но запускать сразу же как появиться возможность
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// Дата и время остановки бота
        /// Если null то будет работать всегда, пока не остановится принудительно 
        /// оператором или использующей его стратегией торговли
        /// /summary>
        public DateTime? StopTime { get; set; }
        /// <summary>
        /// Токен для остановки задачи Task, в которой запущена тактика.
        /// Его нужно десерелизовать
        /// </summary>
        public string CancellationTokenSource { get; set; }
    }

   
}
