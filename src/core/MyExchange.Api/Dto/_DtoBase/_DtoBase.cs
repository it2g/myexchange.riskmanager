﻿using System;
using Abp.Application.Services.Dto;

namespace MyExchange.Api.Dto._DtoBase
{
    /// <summary>
    /// Базовый класс для всех объектов передачи данных
    /// </summary>
    public class DtoBase : EntityDto<Guid>
    {

    }
}
