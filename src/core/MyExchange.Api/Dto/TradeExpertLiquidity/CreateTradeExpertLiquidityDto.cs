﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Runtime.Validation;

namespace MyExchange.Api.Dto.TradeExpertLiquidity
{
   public class CreateTradeExpertLiquidityDto : IShouldNormalize
    {
       public Guid TradeExpertId { get; set; }

       public double Liquidity { get; set; }

       public DateTime Date { get; set; }
        public void Normalize()
        {
           
        }
    }
}
