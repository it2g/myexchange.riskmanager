﻿using System;
using MyExchange.Api.Dto._DtoBase;

namespace MyExchange.Api.Dto.TradeExpertLiquidity
{
   public class TradeExpertLiquidityDto : DtoBase
    {
       public Guid TradeExpertId { get; set; }

       public double Liquidity { get; set; }

       public DateTime Date { get; set; }
    }
}
