﻿using System;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto._DtoBase;

namespace MyExchange.Api.Dto.Signal
{
    /// <summary>
    /// Сигнал на вход в рынок
    /// </summary>
    public class SignalDto : DtoBase
    {
        public decimal Price { get; set; }

        public decimal Stoploss { get; set; }

        public decimal TakeProfit { get; set; }

        public Trends Trend { get; set; }

        /// <summary>
        /// Когда сгенерирован сигнал
        /// </summary>
        public DateTime SignalTime { get; set; }

        public string IndicatorId { get; set; }
        public Guid IndicatorContextId { get; set; }

        public Guid PairId { get; set; }

        public string ExchangeId { get; set; }

        public TimeSpan TimeFrame { get; set; }
    }
}
