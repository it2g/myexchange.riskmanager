﻿namespace MyExchange.Api.Constants
{
    /// <summary>
    /// Статус ордера
    /// </summary>
    public enum OrderStatuses
    {
        New = 1,
        
        RiskManagerProduced = 2,
        
        Opened = 3,

        Tracked = 4,

        Placed = 5,

        Canceled = 6,

        Completed = 7,

        TransferAccountManager = 8,

        ProcessedAccountManager = 9,
    }
}
