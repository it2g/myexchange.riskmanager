﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Bar;

namespace MyExchange.Api.Services.Bl
{
    /// <summary>
    /// Предоставляет свечи записанные в БД
    /// </summary>
    public interface IBarFromDbProvider : IBarProvider
    {
    }
}
