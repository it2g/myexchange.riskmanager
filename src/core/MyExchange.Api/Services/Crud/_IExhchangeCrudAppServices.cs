﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;

namespace MyExchange.Api.Services.Crud
{

    public interface IExhchangeCrudAppServices<TEntityDto, TPrimaryKey, in TGetAllInput, in TCreateInput, in TUpdateInput, in TGetInput, TDeleteInput>:
        IAsyncCrudAppService<TEntityDto, TPrimaryKey,  TGetAllInput, TCreateInput,  TUpdateInput,  TGetInput, TDeleteInput>       
        where TEntityDto : IEntityDto<TPrimaryKey>
        where TUpdateInput : IEntityDto<TPrimaryKey>
        where TGetInput : IEntityDto<TPrimaryKey>
        where TDeleteInput : IEntityDto<TPrimaryKey>
    {
        Task<TEntityDto> Get(TPrimaryKey id);

        Task Delete(TPrimaryKey id);
    }
}
