﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;
using MyExchange.Api.TradingTactic;

namespace MyExchange.Api.Dal.EfConfigurations
{
    public abstract class TradingBotTacticContextBaseMap<TEntity> : BotTacticsContextBaseMap<TEntity>
        where TEntity : TradingTacticContextBase
    {
        public override void Configure(EntityTypeBuilder<TEntity> builder)
        {
            base.Configure(builder);
          
            builder.Property(x => x.TradingStrategyContextId).HasColumnName("trading_strategy_context_id");
            builder.Property(x => x.ExchangeId).HasColumnName("exchange_id");
            builder.Property(x => x.PairId).HasColumnName("pair_id");
            builder.Property(u => u.TradeIndicators).HasColumnName("trade_indicators"); ;

            //TODO builder.Property(x => x.CurrencyValue).HasColumnName("....");
        }
    }
}
