﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;
using MyExchange.Api.TradeExpert;

namespace MyExchange.Api.Dal.EfConfigurations
{
    public abstract class TradeExpertContextBaseMap<TEntity> : BotTacticsContextBaseMap<TEntity>
        where TEntity : TradeExpertContextBase
    {
        public override void Configure(EntityTypeBuilder<TEntity> builder)
        {
            base.Configure(builder);
          
            builder.Property(x => x.ExchangeId).HasColumnName("exchange_id");
            builder.Property(x => x.PairId).HasColumnName("pair_id");
            builder.Property(x => x.TimeFrame).HasColumnName("time_frame");
            builder.Property(x => x.Veracity).HasColumnName("veracity");
        }
    }
}
