﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Entity;
using MyExchange.Api.TradingStrategy;

namespace MyExchange.Api.Dal.EfConfigurations
{
    public abstract class TradingStrategyContextBaseMap<TEntity> : BotTacticsContextBaseMap<TEntity>
        where TEntity : TradingStrategyContextBase
    {
        public override void Configure(EntityTypeBuilder<TEntity> builder)
        {
            base.Configure(builder);
          
            builder.Property(x => x.Duration).HasColumnName("duration");
            builder.Property(x => x.RiskDegree).HasColumnName("risk_degree");
            builder.Property(x => x.UserId).HasColumnName("user_id");
        }
    }
}
