﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class TradeExpertAdviceMap : BaseEntityMap<TradeExpertAdvice>
    {
        public override void Configure(EntityTypeBuilder<TradeExpertAdvice> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.Advice).HasColumnName("advice").IsRequired();
            builder.Property(u => u.Possibility).HasColumnName("possibility").IsRequired();

            builder.ToTable("trade_expert_advices", "core");
        }
    }
}
