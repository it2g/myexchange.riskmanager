﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.EfConfigurations
{
    public class WalletMap : BaseEntityMap<Wallet>
    {
        public override void Configure(EntityTypeBuilder<Wallet> builder)
        {
            base.Configure(builder);
            builder.Property(u => u.UserId).HasColumnName("user_id").IsRequired();
            builder.Ignore(x => x.User);


            builder.ToTable("wallet", "core");
        }
    }
}
