﻿using System;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using MyExchange.Api.Dal;
using MyExchange.Api.Entity;

namespace MyExchange.Dal.Repositories
{
    public class PairRepository : EfCoreRepositoryBase<MyExchangeDbContext, Pair, Guid>, IPairRepository
    {
        public PairRepository(IDbContextProvider<MyExchangeDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
