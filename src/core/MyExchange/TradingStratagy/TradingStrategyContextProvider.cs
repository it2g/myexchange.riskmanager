﻿using MyExchange.Api.TradingStrategy;
using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Dependency;
using JetBrains.Annotations;

namespace MyExchange.TradingStratagy
{
    public class TradingStrategyContextProvider : ITradingStrategyContextProvider
    {
        private readonly  IIocResolver _iocResolver;

        public TradingStrategyContextProvider(IIocResolver iocResolver)
        {
            _iocResolver = iocResolver;
        }

        public TradingStrategyContextBase Get(Guid contextId, string stratagyId)
        {
            var stratagies = _iocResolver.ResolveAll<ITradingStrategy>();

            var stratagy = stratagies.FirstOrDefault(s => s.Id == stratagyId && s.GetContext().Id == contextId);

            return stratagy?.GetContext();
        }
    }
}
