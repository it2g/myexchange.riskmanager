﻿using System;
using System.Linq;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
   public class OpenedOrderCrudAppService : MyExchangeCrudServiceBase<OpenedOrder, OpenedOrderDto, OpenedOrderPagedResultRequestDto, Guid>, IOpenedOrderCrudAppService
   {
        public OpenedOrderCrudAppService(IOpenedOrderRepository repository) : base(repository)
        {

        }
        protected override IQueryable<OpenedOrder> CreateFilteredQuery(OpenedOrderPagedResultRequestDto input)
        {
            var query = base.CreateFilteredQuery(input);
            if(input.Status != null)
            {
                query = query.Where(order => order.Order.OrderStatus == input.Status);
            }
            return query;
        }
    }
}
