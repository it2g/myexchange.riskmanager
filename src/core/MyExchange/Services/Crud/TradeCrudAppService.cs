﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
    public class TradeCrudAppService : MyExchangeCrudServiceBase<Trade, TradeDto, TradePagedResultRequestDto, Guid>, ITradeCrudAppService
    {
        public TradeCrudAppService(ITradeRepository repository) : base(repository)
        {
        }

        public async Task<IList<TradeDto>> AddList(IList<TradeDto> list)
        {
            IList<TradeDto> outputList = new List<TradeDto>();

            foreach (var dto in list)
            {
                var trade = await Create(dto);

                outputList.Add(trade);
            }


            return outputList;
        }

        public Task<TradeDto> GetCurrentAsync(DateTime dateTime)
        {
            return Task.Run(() =>
            {
                var trade = Repository.GetAll().Last(t => t.Date <= dateTime);

                return ObjectMapper.Map<TradeDto>(trade);
            });
        }

        protected override IQueryable<Trade> CreateFilteredQuery(TradePagedResultRequestDto input)
        {
            var query = base.CreateFilteredQuery(input).Where(x=>
                x.ExchangeId == input.ExchangeId && x.PairId == input.PairId );

            if (input.LastRequestTime != null)
            {
                query = query.Where(x => x.CreationTime >= input.LastRequestTime);
            }

            return query;
        }
    }
}
