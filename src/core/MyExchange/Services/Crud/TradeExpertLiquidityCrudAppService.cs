﻿using System;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.TradeExpertLiquidity;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
    public class TradeExpertLiquidityCrudAppService :
        MyExchangeCrudServiceBase<TradeExpertLiquidity, TradeExpertLiquidityDto, TradeExpertLiquidityPagedResultRequestDto, Guid>, ITradeExpertLiquidityCrudAppService
    {
        public TradeExpertLiquidityCrudAppService(ITradeExpertLiquidityRepository repository) : base(repository)
        {

        }

    }
}
