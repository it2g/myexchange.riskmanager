﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyExchange.Api.Dal;
using MyExchange.Api.Dto.Order;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Crud;
using MyExchange.Web.Services;

namespace MyExchange.Services.Crud
{
   public class OrderCrudAppService : MyExchangeCrudServiceBase<Order, OrderDto, OrderPagedResultRequestDto, Guid>, IOrderCrudAppService
    {
        public OrderCrudAppService(IOrderRepository repository) : base(repository)
        {
        }

        public async Task<IList<OrderDto>> AddList(IList<OrderDto> list)
        {
            IList<OrderDto> outputList = new List<OrderDto>();

            foreach (var dto in list)
            {
                var order = await Create(dto);

                outputList.Add(order);
            }
            
            return outputList;
        }

        protected override IQueryable<Order> CreateFilteredQuery(OrderPagedResultRequestDto input)
        {
            var query = base.CreateFilteredQuery(input).Where(x => x.ExchangeId == input.ExchangeId);

            if (input.LastRequestTime != null)
            {
                query = query.Where(x => x.CreationTime >= input.LastRequestTime);
            }

            return query;
        }
    }
}
