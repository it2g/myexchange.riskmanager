﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Bar;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;
using MyExchange.Api.Services.Infrastructure;


namespace MyExchange.Services.Bl
{
    /// <summary>
    /// Предоставляет сервис управления свечами
    /// </summary>
    public class BarFromTradeProvider : IBarProvider
    {
        private ITradeCrudAppService _tradeCrudAppService;
        private ITimer _timer;

        public BarFromTradeProvider([NotNull] ITradeCrudAppService tradeCrudAppService,
            [NotNull] ITimer timer)
        {
            _tradeCrudAppService = tradeCrudAppService ?? throw new ArgumentNullException(nameof(tradeCrudAppService));
            _timer = timer ?? throw new ArgumentNullException(nameof(timer));
        }

        public async Task<IList<BarDto>>  GetList(Guid pairId, string exchangeId, TimeSpan timeFrame, DateTime begin, DateTime end, int countBars = 0)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pairId"></param>
        /// <param name="exchangeId"></param>
        /// <param name="timeFrame"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public async Task<IList<BarDto>> GetList(Guid pairId, string exchangeId, TimeSpan timeFrame, DateTime begin, DateTime end)
        {
            var filter = new TradePagedResultRequestDto()
            {
                ExchangeId = exchangeId,
                PairId = pairId,
                LastRequestTime = begin
            };

            var trades = (await _tradeCrudAppService.GetAll(filter)).Items;

            var bars = new List<BarDto>();

            DateTime slider = begin;

            for (int barNumber = 1; slider < end-TimeSpan.FromTicks(20); barNumber++)
            {
                var barTrades = trades
                                .Where(t => t.Date >= slider && t.Date < slider + timeFrame )
                                .OrderBy(t => t.Date).ToList();
                var bar = new BarDto();

                if (!barTrades.Any())
                {
                    var prevoseBar = bars.LastOrDefault();
                    if (prevoseBar != null)
                    {
                        bar = new BarDto()
                        {
                            TimeFrame = timeFrame,
                            Open = prevoseBar.Close,
                            Close = prevoseBar.Close,
                            Height = prevoseBar.Close,
                            Low = prevoseBar.Close,
                        };
                    }
                    else
                    {
                        //TODO нужна свеча с предыдущего диапазаона
                        throw new Exception("В первой свече нет ни одной сделки, поэтому определить ее не возможно.");
                    }
                }
                else
                {
                    bar = new BarDto()
                    {
                        TimeFrame = timeFrame,
                        Open = barTrades.First().Price,
                        Close = barTrades.Last().Price,
                        Height = barTrades.OrderBy(t => t.Price).Last().Price,
                        Low = barTrades.OrderByDescending(t => t.Price).Last().Price,
                    };
                }
                bars.Add(bar);

                slider += timeFrame;
            }
            return bars;
        }

        /// <summary>
        /// TODO нужен модульный тест для этого метода
        /// </summary>
        public async Task<IList<BarDto>> GetList(Guid pairId, string exchangeId, TimeSpan timeFrame, int countBars = 0)
        {
            var trades = await GetList(pairId, exchangeId, timeFrame,  DateTime.UtcNow - (countBars * timeFrame), DateTime.UtcNow);

            return trades;
        }

        /// <summary>
        /// Определение тренда свечи по некоторым характеристикам
        /// </summary>
        /// <returns></returns>
        private Trends TrendDefine(BarDto barDto)
        {
            if (barDto.Open > barDto.Close)
            {
                return Trends.Downtrend;

            }
            else if(barDto.Open < barDto.Close)
            {
                return Trends.Uptrend;
            }
            
            return Trends.Sideways;
        }
    }
}
