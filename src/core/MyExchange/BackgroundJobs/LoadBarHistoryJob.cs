﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Hangfire;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Internal;
using MyExchange.Api.Dal;
using MyExchange.Api.Data;
using MyExchange.Api.Dto.Bar;
using MyExchange.Api.Dto.Currency;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Entity;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Bl;
using MyExchange.Api.Services.Crud;

namespace MyExchange.BackgroundJobs
{
    /// <summary>
    /// Считывает свечи из файлов для целей тестирования на свечах различных
    /// функциональных механизмов: Adviser, Indicator
    /// </summary>
    [Queue("test_data_load")]
    public class LoadBarHistoryJob : BackgroundJob<LoadBarHistoryJobContext>, ITransientDependency
    {
        private readonly IBarCrudAppService _barCrudAppService;
        private static object lockerCreateBar = new object();

        public LoadBarHistoryJob([NotNull] IBarCrudAppService barCrudAppService)
        {
            _barCrudAppService = barCrudAppService ?? throw new ArgumentNullException(nameof(barCrudAppService));
        }
        [UnitOfWork(IsDisabled = true)]
        public override void Execute(LoadBarHistoryJobContext context)
        {
            StreamReader sr = new StreamReader(context.File);

            while (!sr.EndOfStream)
            {
                string[] barData = sr.ReadLine().Split(',');

                var bar = new BarDto()
                {
                    PairId = context.PairId,
                    ExchangeId = context.ExchangeId,
                    TimeFrame = context.TimeFrame

                };
                bar.BarTime = Convert.ToDateTime(barData[0] + " " + barData[1]);
                bar.Open = Convert.ToDecimal(barData[2].Replace(".", ","));
                bar.Height = Convert.ToDecimal(barData[3].Replace(".", ","));
                bar.Low = Convert.ToDecimal(barData[4].Replace(".", ","));
                bar.Close = Convert.ToDecimal(barData[5].Replace(".", ","));
                bar.Value = Convert.ToDecimal(barData[6].Replace(".", ","));

                lock (lockerCreateBar)
                {
                    bar = _barCrudAppService.Create(bar).Result;
                }
            }

            Debug.WriteLine($"Завершена запись свечей для пары {context.PairId.ToString()} с таймфреймом {context.TimeFrame}");
        }
    }

    public class LoadBarHistoryJobContext
    {
        public string File { get; set; }

        public Guid PairId { get; set; }

        public string ExchangeId { get; set; }

        public TimeSpan TimeFrame { get; set; }
    }
}
