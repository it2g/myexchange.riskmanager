using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace MyExchange.Controllers
{
    public abstract class MyExchangeControllerBase: AbpController
    {
        protected MyExchangeControllerBase()
        {
            LocalizationSourceName = MyExchangeConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
