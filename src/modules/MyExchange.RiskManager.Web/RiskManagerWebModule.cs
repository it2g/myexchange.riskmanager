﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace MyExchange.RiskManager.Web
{
    public class RiskManagerWebModule : AbpModule
    {
        public override void PreInitialize()
        {

        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(RiskManagerWebModule).GetAssembly());
        }

        public override void PostInitialize()
        {

        }
    }
}
