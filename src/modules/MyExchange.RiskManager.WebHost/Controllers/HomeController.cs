using Microsoft.AspNetCore.Mvc;
using MyExchange.Controllers;

namespace MyExchange.RiskManager.WebHost.Controllers
{
    public class HomeController : MyExchangeControllerBase
    {

        public HomeController()
        {
        }

        public IActionResult Index() => Redirect("/swagger");


    }
}
