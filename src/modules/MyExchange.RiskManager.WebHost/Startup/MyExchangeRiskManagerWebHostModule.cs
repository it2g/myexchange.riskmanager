﻿using Abp.Hangfire;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.RemoteEventBus.Kafka;
using MyExchange.RiskManager;
using MyExchange.RiskManager.Web;

namespace MyExchange.RiskManager.WebHost.Startup
{
    [DependsOn(typeof(AbpHangfireAspNetCoreModule),
               typeof(AbpRemoteEventBusKafkaModule),
               typeof(RiskManagerModule),
               typeof(RiskManagerWebModule)
               )]
    public class MyExchangeRiskManagerWebHostModule : AbpModule
    {
        public override void PreInitialize()
        {
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MyExchangeRiskManagerWebHostModule).GetAssembly());
        }

        public override void PostInitialize()
        {
        }
    }
}
