﻿using Abp.Application.Services;
using System;
using System.Threading.Tasks;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.OpenedOrder;

namespace MyExchange.RiskManager.Api
{
    /// <summary>
    /// Риск менеджер который
    /// </summary>
    public interface IRiskManager : IApplicationService
    {
        /// <summary>
        /// Рассчитывает параметры выставляемого ордера
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        Task<OpenedOrderDto> CalculateAsync(OpenedOrderDto order, TimeSpan timeFrame, TradeTypes tradeTypes);
    }
}
