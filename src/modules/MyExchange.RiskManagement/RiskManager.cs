﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.OpenedOrder;
using MyExchange.Api.Entity;
using MyExchange.Api.Services.Bl;
using MyExchange.RiskManager.Api;

namespace MyExchange.RiskManager
{
    /// <summary>
    /// Менеджер ордеров.
    /// </summary>
    public class RiskManager : IRiskManager
    {
        private readonly IBarProvider _barProvider;

        public RiskManager([NotNull] IBarProvider barProvider)
        {
            _barProvider = barProvider ?? throw new ArgumentNullException(nameof(barProvider));
        }
        
        //Метод реализован как заглушка. Нужно переделывать
        public async Task<OpenedOrderDto> CalculateAsync(OpenedOrderDto order, TimeSpan timeFrame, TradeTypes tradingType)
        {
            var bars = await _barProvider.GetList(order.Order.PairId, order.Order.ExchangeId, timeFrame, 20);

            return order;
        }

    }
}
